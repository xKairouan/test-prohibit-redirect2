// -*- coding: utf-8 -*-

'use strict';

(function () {
    var requesting = false;
    var test_request = function (url, upload_events_flag) {
        if (requesting) {
            setTimeout(function () {
                test_request(url, upload_events_flag);
            }, 1000);
            return;
        }
        var messages = [
            '--- requesting ---',
            'to                : '+ url,
            'upload events flag: ' + upload_events_flag
        ];
        var req = new XMLHttpRequest();
        if (upload_events_flag) {
            req.upload.addEventListener('load', function (e) {}, false);
        }
        req.addEventListener('load', function (e) {
            messages.push('redirect          : succeeded');
            console.log(messages.join('\n'));
        }, false);
        req.addEventListener('error', function (e) {
            messages.push('redirect          : failed');
            console.log(messages.join('\n'));
        }, false);
        req.open('GET', url, true);
        req.send(null);
    };

    var url = document.querySelector('a[rel="next"]').href;
    test_request(url, true);
    test_request(url, false);
}());

/*
 * Environment:
 *   UA: Mozilla/5.0 (X11; Linux i686; rv:11.0a2) Gecko/20120131 Firefox/11.0a2Mozilla/5.0 (X11; Linux i686; rv:11.0a2) Gecko/20120131 Firefox/11.0a2
 *   Add-on SDK: 1.5b3
 * Result (in Web Console):
 *   --- requesting ---
 *   to                : http://misc-xkyrgyzstan.dotcloud.com/aptests/open_redirector?url=http%3A%2F%2Fpure-stream-9966.herokuapp.com%2Fredirect%2Fng%2Fcors
 *   upload events flag: true
 *   redirect          : failed
 *   --- requesting ---
 *   to                : http://misc-xkyrgyzstan.dotcloud.com/aptests/open_redirector?url=http%3A%2F%2Fpure-stream-9966.herokuapp.com%2Fredirect%2Fng%2Fcors
 *   upload events flag: false
 *   redirect          : succeeded
 *
 * Result (in Add-on):
 *   --- requesting ---
 *   to                : http://misc-xkyrgyzstan.dotcloud.com/aptests/open_redirector?url=http%3A%2F%2Fpure-stream-9966.herokuapp.com%2Fredirect%2Fng%2Fcors
 *   upload events flag: true
 *   redirect          : failed
 *   --- requesting ---
 *   to                : http://misc-xkyrgyzstan.dotcloud.com/aptests/open_redirector?url=http%3A%2F%2Fpure-stream-9966.herokuapp.com%2Fredirect%2Fng%2Fcors
 *   upload events flag: false
 *   redirect          : succeeded
 */
